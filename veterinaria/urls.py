from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from core.views import HomeView
from .views import ReservaMedicaCreateView, ReservaMedicaListView, ReservaMedicaUpdateView, ReservaMedicaDeleteView
from core.views import FuncionariosView, InventarioView, LoginView
app_name = 'veterinaria'

urlpatterns = [
    path('',HomeView.as_view(), name='home' ),
    path('reservasmedicas/', ReservaMedicaListView.as_view(), name='reservasmedicas' ),
    path('reservasmedicas/create/', ReservaMedicaCreateView.as_view(), name='reservasmedicascreate' ),
    path('reservasmedicas/<int:pk>/reservasmedicasupdate/', ReservaMedicaUpdateView.as_view(), name='reservasmedicasupdate'),
    path('reservasmedicas/<int:pk>/reservasmedicasdelete/', ReservaMedicaDeleteView.as_view(), name='reservasmedicasdelete'),
    path('funcionarios/', FuncionariosView.as_view(), name='funcionarios'),
    path('inventario/', InventarioView.as_view(), name='inventario'),
    path('cerrarsesion/', LoginView.as_view(), name='cerrarsesion')

]

urlpatterns += staticfiles_urlpatterns()