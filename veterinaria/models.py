from django.db import models

# Create your models here.

class UserAccount(models.Model):
    user = models.CharField(max_length=10)
    password = models.CharField(max_length=10)

    def __str__(self):
        return self.user

class ReservaMedica(models.Model):
    nombreCliente = models.TextField()
    apellidoCliente = models.TextField()
    rutCliente = models.TextField()
    telefonoCliente = models.IntegerField()
    direccionCliente = models.TextField()
    fechaReserva = models.DateField()
    horaReserva = models.TimeField()
    mascotaCliente = models.TextField()

class Funcionario(models.Model):
    nombreFuncionario = models.TextField()
    apellidoFuncionario = models.TextField()
    rutFuncionario = models.TextField()
    telefonoFuncionario = models.TextField()
    direccionFuncionario = models.TextField()
    contactoEmergencia = models.TextField()
    telefonoEmergencia = models.TextField()

class material(models.Model):
    nombreMaterial: models.TextField()
    fechaIngresoMaterial: models.TextField()
    numeroSerieMaterial:models.TextField()
    cantidadMaterial: models.TextField()
