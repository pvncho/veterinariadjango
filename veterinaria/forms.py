from django import forms 
from .models import ReservaMedica

class ReservaMedicaCreateForm(forms.ModelForm):
    class Meta:
        model = ReservaMedica
        fields = ('nombreCliente', 'apellidoCliente', 'rutCliente', 'telefonoCliente','direccionCliente','fechaReserva', 'horaReserva', 'mascotaCliente')
        widgets = {
            'nombreCliente': forms.TextInput(attrs={'class':'form-control'}),
            'apellidoCliente': forms.TextInput(attrs={'class':'form-control'}),
            'rutCliente': forms.TextInput(attrs={'class':'form-control'}),
            'telefonoCliente': forms.TextInput(attrs={'class':'form-control'}),
            'direccionCliente': forms.TextInput(attrs={'class':'form-control'}),
            'fechaReserva': forms.TextInput(attrs={'class':'form-control'}),
            'horaReserva': forms.TextInput(attrs={'class':'form-control'}),
            'mascotaCliente': forms.TextInput(attrs={'class':'form-control'})
            
        }

class ReservaMedicaUpdateForm(forms.ModelForm):
    class Meta:
        model = ReservaMedica
        fields = ('nombreCliente', 'apellidoCliente', 'rutCliente', 'telefonoCliente','direccionCliente','fechaReserva', 'horaReserva', 'mascotaCliente')
        widgets = {
            'nombreCliente': forms.TextInput(attrs={'class':'form-control'}),
            'apellidoCliente': forms.TextInput(attrs={'class':'form-control'}),
            'rutCliente': forms.TextInput(attrs={'class':'form-control'}),
            'telefonoCliente': forms.TextInput(attrs={'class':'form-control'}),
            'direccionCliente': forms.TextInput(attrs={'class':'form-control'}),
            'fechaReserva': forms.TextInput(attrs={'class':'form-control'}),
            'horaReserva': forms.TextInput(attrs={'class':'form-control'}),
            'mascotaCliente': forms.TextInput(attrs={'class':'form-control'})
            
        }


