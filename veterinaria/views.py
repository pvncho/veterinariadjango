from django import forms
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.views.generic import DeleteView, UpdateView, View
from veterinaria.forms import ReservaMedicaCreateForm, ReservaMedicaUpdateForm
from veterinaria.models import ReservaMedica


# Create your views here.
class ReservaMedicaListView(View):
    def get(self, request, *args, **kwargs):
        reservasmedicas=ReservaMedica.objects.all()
        context={
            'reservasmedicas' : reservasmedicas
        }
        
        return render(request, 'reservasmedicas.html', context)

    

class ReservaMedicaCreateView(View):
    def get(self, request):
        form = ReservaMedicaCreateForm()
        context = {
            'form':form
        }
        return render(request, 'reservasmedicascreate.html', context)

    def post(self, request):
        form = ReservaMedicaCreateForm(request.POST)
        if form.is_valid():
            nombreCliente = form.cleaned_data.get('nombreCliente')
            apellidoCliente = form.cleaned_data.get('apellidoCliente')
            rutCliente = form.cleaned_data.get('rutCliente')
            telefonoCliente = form.cleaned_data.get('telefonoCliente')
            direccionCliente = form.cleaned_data.get('direccionCliente')
            fechaReserva = form.cleaned_data.get('fechaReserva')
            horaReserva = form.cleaned_data.get('horaReserva')
            mascotaCliente = form.cleaned_data.get('mascotaCliente')
            form.save()
        return redirect('veterinaria:reservasmedicas')

class ReservaMedicaUpdateView(UpdateView):
    model = ReservaMedica
    form_class = ReservaMedicaUpdateForm
    template_name= 'reservasmedicasupdate.html'

    def get_success_url(self):
        pk = self.kwargs['pk']
        return reverse_lazy('veterinaria:reservasmedicas')

class ReservaMedicaDeleteView(DeleteView):
    model = ReservaMedica
    template_name = 'reservasmedicasdelete.html'
    success_url = reverse_lazy('veterinaria:reservasmedicas')