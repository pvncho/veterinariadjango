from django.shortcuts import render
from django.views.generic import View, TemplateView

# Create your views here.

class HomeView(View):
    def get(self, request, *args, **kwargs):
        context={}
        return render(request, 'index.html', context)

class LoginView(View):
    def get(self, request, *args, **kwargs):
        context={}
        return render(request, 'login.html', context)

class FuncionariosView(View):
    def get(self, request, *args, **kwargs):
        context={}
        return render(request, 'funcionarios.html', context)

class InventarioView(View):
    def get(self, request, *args, **kwargs):
        context={}
        return render(request, 'inventario.html', context)
